from google.cloud import bigquery
from google.oauth2 import service_account

credentials = service_account.Credentials.from_service_account_file('/Users/peining/gcp-credential/self-service-data.json')
project_id = 'self-service-data'
client = bigquery.Client(credentials= credentials,project=project_id)

import pandas as pd
csv = pd.read_csv('Internal Research Sample Account.csv')

accts = csv['Account ID'].values.tolist()

for acct in accts:

    query = (
    f"""
        DROP TABLE IF EXISTS `self-service-data.Daily_User_Summary_Enhanced.{acct}`;
        CREATE TABLE `self-service-data.Daily_User_Summary_Enhanced.{acct}`
          (
              account_id INT64,
              user_id INT64,
              user_name STRING,
              user_primary_group_name STRING,
              productive_hrs_day_goal FLOAT64,
              focused_hrs_day_goal FLOAT64,
              local_date DATE,
              day_of_week STRING,
              day_type STRING,
              activity_week DATE,
              productive_activity_count INT64,
              unproductive_activity_count INT64,
              undefined_activity_count INT64,
              total_activity_count INT64,
              collaboration_activity_count INT64,
              attention_shift_activity_count INT64,
              productive_active_duration_seconds INT64,
              productive_passive_duration_seconds INT64,
              unproductive_active_duration_seconds INT64,
              unproductive_passive_duration_seconds INT64,
              undefined_active_duration_seconds INT64,
              undefined_passive_duration_seconds INT64,
              active_duration_seconds INT64,
              total_duration_seconds INT64,
              focused_duration_seconds INT64,
              collaboration_duration_seconds INT64,
              multitasking_duration_seconds INT64,
              break_count INT64,
              break_duration_seconds INT64,
              non_business_activity_count INT64,
              non_business_duration_seconds INT64,
              productive_session_count INT64,
              productive_session_duration_seconds INT64,
              focused_session_count INT64,
              focused_session_duration_seconds INT64,
              first_activity_datetime DATETIME,
              last_activity_datetime DATETIME,
              first_activity_minutes_in_day INT64,
              last_activity_minutes_in_day INT64,
              utilization_level STRING
          );


        INSERT `self-service-data.Daily_User_Summary_Enhanced.{acct}`
        WITH daily_user_summary_enhanced_{acct} AS(
            WITH user_goal AS(
                  SELECT
                      daily.account_id,
                      daily.user_name,
                      user_group.group_name AS user_primary_group_name,
                      daily.activity_date,
                      MAX(daily.user_id) AS user_id,
                      COALESCE(MAX(productivity_targets.target_value), 6.0) AS productive_hrs_day_target_value,
                      COALESCE(MAX(focus_targets.target_value), 3.0) AS focused_hrs_day_target_value,
                      CASE WHEN (SUM(daily.productive_active_duration_seconds + daily.productive_passive_duration_seconds)/3600) > (COALESCE(MAX(productivity_targets.target_value), 6.0)) * (1+(30/100)) THEN 'Overutilized'
                            WHEN (SUM(daily.productive_active_duration_seconds + daily.productive_passive_duration_seconds)/3600) < (COALESCE(MAX(productivity_targets.target_value), 6.0)) * (1-(30/100)) THEN 'Underutilized'
                            ELSE 'Healthy'
                        END AS utilization_level
                  FROM
                    `accelerate-playground.activinsights_daily_user_summary.{acct}` daily
                  JOIN `accelerate-playground.activinsights_user.{acct}` user
                    ON daily.account_id = user.account_id AND daily.user_name = user.user_name
                  JOIN `accelerate-playground.activinsights_group.{acct}` user_group
                    ON daily.account_id = user_group.account_id AND user.primary_group_id = user_group.group_id
                  LEFT JOIN `accelerate-playground.activinsights_user_group_targets_vw.{acct}` productivity_targets
                    ON daily.account_id = productivity_targets.account_id AND user.primary_group_id = productivity_targets.group_id AND productivity_targets.metric_name = 'productive_hrs_day'
                  LEFT JOIN `accelerate-playground.activinsights_user_group_targets_vw.{acct}` focus_targets
                    ON daily.account_id = focus_targets.account_id AND user.primary_group_id = focus_targets.group_id AND focus_targets.metric_name = 'focused_hrs_day'

                  WHERE
                    daily.account_id = {acct}
                  GROUP BY
                    1, 2, 3, 4
                  ORDER BY 4 DESC
              )


            SELECT
                 daily.account_id
                ,daily.user_id
                ,daily.user_name
                ,goal.user_primary_group_name
                ,productive_hrs_day_target_value AS productive_hrs_day_goal
                ,focused_hrs_day_target_value AS focused_hrs_day_goal
                ,daily.activity_date AS local_date
                ,CASE
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 2 then "Monday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 3 then "Tuesday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 4 then "Wednesday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 5 then "Thursday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 6 then "Friday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 7 then "Saturday"
                  WHEN EXTRACT(DAYOFWEEK from daily.activity_date) = 1 then "Sunday"
                END AS day_of_week
                ,CASE
                  WHEN EXTRACT(DAYOFWEEK FROM daily.activity_date) in (1,7) THEN 'Weekend'
                    ELSE 'Weekday'
                END AS day_type
                ,DATE_ADD(LAST_DAY(daily.activity_date, WEEK), INTERVAL -6 DAY) as activity_week
                ,daily.productive_activity_count
                ,daily.unproductive_activity_count
                ,daily.undefined_activity_count
                ,daily.total_activity_count
                ,daily.collaboration_activity_count
                ,daily.context_switch_activity_count AS attention_shift_activity_count
                ,daily.productive_active_duration_seconds
                ,daily.productive_passive_duration_seconds
                ,daily.unproductive_active_duration_seconds
                ,daily.unproductive_passive_duration_seconds
                ,daily.undefined_active_duration_seconds
                ,daily.undefined_passive_duration_seconds
                ,daily.active_duration_seconds
                ,daily.total_duration_seconds
                ,(daily.productive_active_duration_seconds + daily.productive_passive_duration_seconds - daily.collaboration_duration_seconds - daily.context_switch_duration_seconds) AS focused_duration_seconds
                ,daily.collaboration_duration_seconds
                ,daily.context_switch_duration_seconds AS multitasking_duration_seconds
                ,daily.break_count
                ,daily.break_duration_seconds
                ,(daily.productive_activity_count + daily.unproductive_activity_count + daily.undefined_activity_count) AS non_business_activity_count
                ,(daily.unproductive_active_duration_seconds + daily.unproductive_passive_duration_seconds + daily.undefined_active_duration_seconds + daily.undefined_passive_duration_seconds) AS non_business_duration_seconds
                ,daily.productive_session_count
                ,daily.productive_session_duration_seconds
                ,daily.focused_session_count
                ,daily.focused_session_duration_seconds
                ,daily.first_activity_datetime
                ,daily.last_activity_datetime
                ,daily.first_activity_minutes_in_day
                ,daily.last_activity_minutes_in_day
                ,goal.utilization_level
            FROM user_goal AS goal
            LEFT JOIN `accelerate-playground.activinsights_daily_user_summary.{acct}` daily ON daily.account_id = goal.account_id AND daily.user_name = goal.user_name AND daily.activity_date = goal.activity_date
            ORDER BY 7 DESC

               
        )
        SELECT * FROM daily_user_summary_enhanced_{acct};

        """
    )
    query_job = client.query(query)
    result = query_job.result()





for acct in accts:
        query = (
        f"""
            DROP TABLE IF EXISTS `self-service-data.Daily_Application_Summary.{acct}`;
            CREATE TABLE `self-service-data.Daily_Application_Summary.{acct}`
              (
                  account_id INT64,
                  user_id INT64,
                  user_name STRING,
                  user_primary_group_name STRING,
                  local_date DATE,
                  day_type STRING,
                  day_of_week STRING,
                  activity_week DATE,
                  computer_id INT64,
                  passive_type_id INT64,
                  executable STRING,
                  application STRING,
                  browser_site STRING,
                  application_or_site STRING,
                  host STRING,
                  website STRING,
                  activity_type STRING,
                  category STRING,
                  productivity STRING,
                  duration_seconds FLOAT64

              );


            INSERT `self-service-data.Daily_Application_Summary.{acct}`
            WITH daily_application_summary_{acct} AS(
              SELECT
                   activities.account_id
                  ,activities.user_id
                  ,activities.user_name
                  ,user_group.group_name AS user_primary_group_name
                  ,activities.activity_date AS local_date
                  ,CASE WHEN EXTRACT(DAYOFWEEK FROM activities.activity_date) IN (1,7) THEN 'Weekend' ELSE 'Weekday' END AS day_type
                  ,CASE
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 2 then "Monday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 3 then "Tuesday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 4 then "Wednesday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 5 then "Thursday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 6 then "Friday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 7 then "Saturday"
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 1 then "Sunday"
                    END AS day_of_week
                  ,DATE_ADD(LAST_DAY(activities.activity_date, WEEK), INTERVAL -6 DAY) as activity_week
                  ,activities.computer_id
                  ,activities.passive_type_id
                  ,activities.executable
                  ,activities.description AS application
                    ,CONCAT(COALESCE(activities.description,activities.executable),'(',activities.host,')') AS browser_site
                  ,CASE WHEN activities.host IS NULL THEN activities.application ELSE activities.host END AS application_or_site
                    ,activities.host
                    ,activities.website
                    ,activities.activity_type
                  ,activities.category
                  ,activities.productivity
                  ,(COALESCE(ROUND(COALESCE(CAST( ( SUM(DISTINCT (CAST(ROUND(COALESCE(activities.duration_seconds ,0)*(1/1000*1.0), 9) AS NUMERIC) + (cast(cast(concat('0x', substr(to_hex(md5(CAST(activities.row_id  AS STRING))), 1, 15)) as int64) as numeric) * 4294967296 + cast(cast(concat('0x', substr(to_hex(md5(CAST(activities.row_id  AS STRING))), 16, 8)) as int64) as numeric)) * 0.000000001 )) - SUM(DISTINCT (cast(cast(concat('0x', substr(to_hex(md5(CAST(activities.row_id  AS STRING))), 1, 15)) as int64) as numeric) * 4294967296 + cast(cast(concat('0x', substr(to_hex(md5(CAST(activities.row_id  AS STRING))), 16, 8)) as int64) as numeric)) * 0.000000001) )  / (1/1000*1.0) AS FLOAT64), 0), 6), 0)) AS duration_seconds
              FROM `accelerate-playground.activinsights_user.{acct}` user
              LEFT JOIN `accelerate-playground.activinsights_group.{acct}` user_group
              ON user.account_id = user_group.account_id AND user.primary_group_id = user_group.group_id
              LEFT JOIN `accelerate-playground.activinsights_daily_activity_summary.{acct}` AS activities
              ON activities.account_id = user.account_id AND activities.user_id = user.user_id
              GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
              ORDER BY 5 DESC
            )
            SELECT * FROM daily_application_summary_{acct};

            """
        )
        query_job = client.query(query)
        result = query_job.result()

